﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KisilikTesti
{
    public partial class frmYorum : Form
    {
        public static int not1, not2, not3, not4;
        int[] dizi = new int[4]{ not1, not2, not3, not4 };
        public int[] dizi2 = new int[4];

        private void button1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Çıkmak istediğinizden emin misiniz ?","Dikkat!!!",MessageBoxButtons.YesNo,MessageBoxIcon.Warning)==DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        int hangisecenek;

        public frmYorum()
        {
            InitializeComponent();
        }        

        private void frmYorum_Load(object sender, EventArgs e)
        {
            int enbuyuk = dizi2[0];
            for (int i = 1; i < dizi2.Length; i++)
            {
                if (dizi2[i] > enbuyuk)
                {
                    enbuyuk = dizi2[i];
                    hangisecenek = i;
                }
            }
            switch(hangisecenek)
            {
                case 0:
                    {
                        lblBaslik.Text = "Sarı Karakter";
                        richTextBox1.Text="- Sarı karakterli kişiler, bu 4 ana kategori içinde en hareketli karakterdir."+Environment.NewLine+ "- İlgi çekici ve karizmatik yapılıdırlar." + Environment.NewLine + "- Dışarıdan bakılınca sevimli, kıpır kıpır, hikayeler anlatıp güldüren ve gülen birini görüyorsanız onları hemen tanırsınız." + Environment.NewLine + "- Birebir ilişkilerde çok aktiftirler." + Environment.NewLine + "- Yabancılarla kolaylıkla ve özel bir çaba göstermeksizin ilişki kurarlar." + Environment.NewLine + "- Geniş bir tanıdık çevreleri vardır." + Environment.NewLine + "- Hayaller kurmaya yatkındırlar." + Environment.NewLine + "- Vizyonları çok geniştir." + Environment.NewLine + "- Bir organizasyon içerisinde büyük projeler hayal edebilen, çok daha büyük ve parlak bir geleceği düşünebilen kişilerdir." + Environment.NewLine + "- Ancak bunları hayata geçirme konusunda büyük sıkıntılar yaşayabilirler." + Environment.NewLine + "- Bu karakter grubunun genel özellikleri; girişkendirler, ayrıntılar üzerinde düşünmezler, çalışma alanları dağınıktır, onları toparlayacak birine ihtiyaç duyarlar." + Environment.NewLine + "- Eğlenceli ve hareketlidirler, mutluluk ve enerji yayarlar, dikkatsizdirler, hazır cevaptırlar, ikna edicidirler, sorunlara ilginç çözümler bulurlar, meraklıdırlar.";
                        richTextBox1.BackColor = Color.Yellow;
                        break;
                    }
                case 1:
                    {
                        lblBaslik.Text = "Mavi Karakterli Kişiler";
                        richTextBox1.Text = "- Mükemmeliyetçidirler." + Environment.NewLine + "- Duruşları, kıyafetleri ve her şeyleriyle çok düzenlidirler." + Environment.NewLine + "- Her şeyleri uyum içinde görünür.Kurallara çok bağlıdırlar." + Environment.NewLine + "- Çalışma ortamları tam anlamıyla mükemmel ve profesyoneldir." + Environment.NewLine + "- Her şeyleri planlı ve programlıdır." + Environment.NewLine + "- Hassas insanlardır." + Environment.NewLine + "- Ayrıntılar konusunda titiz davranırlar." + Environment.NewLine + "- Çoğu zaman karar vermede büyük sıkıntılar yaşarlar." + Environment.NewLine + "- Genelde yalnızlıktan hoşlanırlar." + Environment.NewLine + "- Konuştuklarında ses tonları alçaktır." + Environment.NewLine + "- Konuşma hızları düşüktür." + Environment.NewLine + "- Duygularını açıkça ortaya koymazlar, mantık ağırlıklıdırlar." + Environment.NewLine + "- Heyecanları ve duyguları ölçülüdür." + Environment.NewLine + "- Bir olumsuzluk esnasında sakin ve akılcı olabilirler." + Environment.NewLine + "- Kuralcı olduklarından kolay kolay değişmek istemezler." + Environment.NewLine + "- Daima tedbirlidirler." + Environment.NewLine + "- Olayların olumsuz yönlerini görüp kaygılanırlar." + Environment.NewLine + "- Detaycıdırlar." + Environment.NewLine + "- Meraklıdırlar, araştırmacıdırlar." + Environment.NewLine + "- Riskleri önceden görürler." + Environment.NewLine + "- Ciddi ve ağır başlıdırlar." + Environment.NewLine + "- Standartları yüksektir." + Environment.NewLine + "- Yeteneklidirler.";
                        richTextBox1.BackColor = Color.Blue;
                        break;
                    }
                case 2:
                    {
                        lblBaslik.Text = "Kırmızı Karakterli Kişiler";
                        richTextBox1.Text = "- Güçlü kararlı tiplerdir." + Environment.NewLine + "- Az laf, çok iş derler." + Environment.NewLine + "- Sonuç odaklıdırlar." + Environment.NewLine + "- Herkesin onlar gibi düşünüp hareket etmesini isterler." + Environment.NewLine + "- Gereksiz konuşmalardan ve işlerden hoşlanmazlar." + Environment.NewLine + "- Hep yapacak işleri ve alınmış kararları vardır." + Environment.NewLine + "- Önemli olan onlara göre işin özünü anlamak ve sonucu elde etmektir." + Environment.NewLine + "- Bu yüzden kararlarını uygularken gözleri başka bir şey görmez." + Environment.NewLine + "- Yüksek; ancak gerçekçi hedefler belirlemekten ve bunları gerçekleştirmeye çalışmaktan büyük zevk alırlar." + Environment.NewLine + "- Son derece bağımsız insanlardır. " + Environment.NewLine + "- Zaman yönetimi konusunda üstün bir becerileri vardır." + Environment.NewLine + "- Konuşması hızlı ve tempoludur. " + Environment.NewLine + "- Çalışma ortamları, ne çok dağınık ne de çok düzenlidir." + Environment.NewLine + "- Hep dik durmaya çalışırlar, kimseden yardım istemeyi sevmezler." + Environment.NewLine + "- Kendilerini daima haklı görme eğilimleri vardır. " + Environment.NewLine + "- Eleştiriden pek hoşlanmazlar." + Environment.NewLine + "- Kararlı ve iş bitiricidirler. " + Environment.NewLine + "- Ani kararlarıyla insanları şaşırtabilirler. " + Environment.NewLine + "- İşlerine aşırı odaklanırlar; fakat sonuçlar konusunda yeterli değerlendirmeyi yapamayabilirler. " + Environment.NewLine + "- Lider özelliklidirler. " + Environment.NewLine + "- Özgüvenleri yüksektir.";
                        richTextBox1.BackColor = Color.Red;
                        break;
                    }
                case 3:
                    {
                        lblBaslik.Text = "Yeşil Karakterli Kişiler";
                        richTextBox1.Text = "- Orta yollu tiplerdir." + Environment.NewLine + "- Dikkat çekici hatalarda bulunmazlar. " + Environment.NewLine + "- En belirgin yönleri her yerde mutlu olabilmeleri ve hallerinden memnuniyet duymalarıdır." + Environment.NewLine + "- Sabırlıdırlar." + Environment.NewLine + "- Ayırt edilmesi en güç olan ve zor anlaşılan kişilerdir." + Environment.NewLine + "- İçten içe çok inatçıdırlar. " + Environment.NewLine + "- Projeleri yarım bırakabilirler." + Environment.NewLine + "- Pek fazla kimseye hayır diyemezler." + Environment.NewLine + "- Barışçıl ve fedakardırlar." + Environment.NewLine + "- En çok önemsedikleri şey çevreden saygı görmektir." + Environment.NewLine + "- Daha duygulu ve heyecanlıdırlar. " + Environment.NewLine + "- Gruplarla uyumludurlar." + Environment.NewLine + "- Kendilerini ön plana çıkarmaya çalışmazlar." + Environment.NewLine + "- Başka insanların katkılarındaki değeri görme konusunda oldukça yeteneklidirler. " + Environment.NewLine + "- Uyumlu ilişkilere çok değer verirler." + Environment.NewLine + "- Başka insanların hislerine karşı duyarlıdırlar." + Environment.NewLine + "- Çok hareketli ortamları sevmezler. " + Environment.NewLine + "- İnce ve düşündürücü espiri  anlayışına sahiptirler. " + Environment.NewLine + "- Kendileriyle barışıktırlar. " + Environment.NewLine + "- Çok rahattırlar. " + Environment.NewLine + "- Çatışmadan çok çekinirler." + Environment.NewLine + "- Doğal arabulucudurlar. " + Environment.NewLine + "- Çok sabırlıdırlar. " + Environment.NewLine + "- İçlerinde fırtınalar koparken dışarıdan sakin görünebilirler. " + Environment.NewLine + "- Kimseyi kırmak istemezler.";
                        richTextBox1.BackColor = Color.Green;
                        break;
                    }
                default:
                    {
                        richTextBox1.Text = "Bir hata oldu";
                        richTextBox1.BackColor = Color.Silver;
                        break;
                    }
            }
        }
    }
}
