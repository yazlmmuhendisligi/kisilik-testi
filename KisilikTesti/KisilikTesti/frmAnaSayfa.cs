﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KisilikTesti
{
    public partial class frmAnaSayfa : Form
    {
        public frmAnaSayfa()
        {
            InitializeComponent();
        }
        //Buton 2 ye tıklandığı zaman gerçekleşecek olayları yazıyoruz
        private void button2_Click(object sender, EventArgs e)
        {
            //Ekrana message box içinde bir uyarı verip bunu kontrol ediyorum
            if(MessageBox.Show("Çıkmak istediğinizden emin misiniz ?","UYARI!!!",MessageBoxButtons.YesNo,MessageBoxIcon.Warning)==DialogResult.Yes)
            {
                //Eger cevan-bımız evet ise programdan çıkıyorum
                Application.Exit();
            }

        }
        //Buton1e bastığımızda olacakları yazıyoruz
        private void button1_Click(object sender, EventArgs e)
        {
            //Bu formu kapatıp soru 1 formunu açacağız
            frmSoru1 frm = new frmSoru1();
            this.Hide();
            frm.Show();

        }

        private void frmAnaSayfa_Load(object sender, EventArgs e)
        {

        }
    }
}
