﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KisilikTesti
{

    public partial class frmSoru1 : Form
    {
        float deg1;
        float deg2;
        float deg3;
        float deg4;
        int toplamsonuc = 0;
        int[] x_sayisi = new int[15] ;
        int cevap_Sayisi = 0;
        bool cift_x = false;

        #region Tanımlamalar

        int enbuyuk = 0;
        int siklar;

        int toplamA = 0;
        int toplamB = 0;
        int toplamC = 0;
        int toplamD = 0;
        string[] Acevap = new string[15];
        string[] Bcevap = new string[15];
        string[] Ccevap = new string[15];
        string[] Dcevap = new string[15];
        string[] Asikki = new string[15];
        string[] Bsikki = new string[15];
        string[] Csikki = new string[15];
        string[] Dsikki = new string[15];
        string[] SoruSikki = new string[15]; 
        #endregion


        int toplamXSayisi = 0;
        int kacincisoru = 0;

        public frmSoru1()
        {
            InitializeComponent();
        }
        private void frmSoru1_Load(Object sender, EventArgs e)  // FORM LOAD
        {
                       
            btnBitir.Hide();
            x_sayisi_atama();// x sayılarını default atama
          
            
            SorulariYaz();
            richTextBox1.Text = SoruSikki[0];
            richTextBox_A.Text = Asikki[0];
            richTextBox_B.Text = Bsikki[0];
            richTextBox_C.Text = Csikki[0];
            richTextBox_D.Text = Dsikki[0];
            for (int i = 0; i < 15; i++)
            {
                Acevap[i] = "";
                Bcevap[i] = "";
                Ccevap[i] = "";
                Dcevap[i] = "";
            }
        }

        private void button_NEXT_Click(object sender, EventArgs e)
        {
            cevap_Sayisi = 0;
            cift_x = false;
            toplamXSayisi = txtCevapA.Text.Length + txtCevapB.Text.Length + txtCevapC.Text.Length + txtCevapD.Text.Length;
            if (toplamXSayisi == 0 || toplamXSayisi < 2 || toplamXSayisi > 3)
            {
                MessageBox.Show("X Sayısı girerken hata yaptınız.", "Uyarı!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
            else
            {

                kacincisoru++;
                Soru_label.Text = "Soru " + (kacincisoru + 1) + ")";
                richTextBox1.Text = SoruSikki[kacincisoru];
                richTextBox_A.Text = Asikki[kacincisoru];
                richTextBox_B.Text = Bsikki[kacincisoru];
                richTextBox_C.Text = Csikki[kacincisoru];
                richTextBox_D.Text = Dsikki[kacincisoru];
                txtCevapA.Text = Acevap[kacincisoru];
                txtCevapB.Text = Bcevap[kacincisoru];
                txtCevapC.Text = Ccevap[kacincisoru];
                txtCevapD.Text = Dcevap[kacincisoru];

                if (kacincisoru == 14)
                {
                    button_NEXT.Hide();
                }
                button_BACK.Show();
            }

           
        }

        private void button_BACK_Click(object sender, EventArgs e)
        {
           
            kacincisoru--;

            Soru_label.Text = "Soru " + (kacincisoru + 1) + ")";

            richTextBox1.Text = SoruSikki[kacincisoru];
            richTextBox_A.Text = Asikki[kacincisoru];
            richTextBox_B.Text = Bsikki[kacincisoru];
            richTextBox_C.Text = Csikki[kacincisoru];
            richTextBox_D.Text = Dsikki[kacincisoru];
            txtCevapA.Text = Acevap[kacincisoru];
            txtCevapB.Text = Bcevap[kacincisoru];
            txtCevapC.Text = Ccevap[kacincisoru];
            txtCevapD.Text = Dcevap[kacincisoru];

            if (kacincisoru == 0)
            {
                button_BACK.Hide();
            }
            button_NEXT.Show();

           
        }
        public void SorulariYaz()
        {
            SoruSikki[0] = " Aşağıdaki beyitlerden hangisi sizi daha iyi anlatır ?";
            SoruSikki[1] = " Genellikle hangi tempoda ve nasıl konuşursunuz?";
            SoruSikki[2] = " Bir işe motive olmanızı sağlayan en önemli unsur hangisidir? ";
            SoruSikki[3] = " Çalışma tarzınız hangisine uygundur?  ";
            SoruSikki[4] = " Çalışma temponuzu nasıl değerlendiriyorsunuz?";
            SoruSikki[5] = " Hangisi sizi daha çok rahatsız eder?";
            SoruSikki[6] = " Bulunduğunuz gruplarda hangi konumda daha başarılı olursunuz?";
            SoruSikki[7] = " Hangisi sizi daha çok strese sokar?";
            SoruSikki[8] = " Bir öğrenci olsanız ve öğretmeniniz sınav kağıdınızı ikinci defa incelediğinde puanınızı artırdığını söylese, nasıl bir tepki verirsiniz?";
            SoruSikki[9] = " Saatler sürecek bir iş toplantısına katılmanız gerektiğinde aşağıdakilerden hangisini benimsersiniz?";
            SoruSikki[10] = " Kendinizde gördüğünüz en zayıf yönünüz hangisidir?";
            SoruSikki[11] = " Kendinizde gördüğünüz en güçlü yönünüz hangisidir?";
            SoruSikki[12] = " Aşağıdaki ifadelerden hangisi sizi daha iyi tanımlar ?";
            SoruSikki[13] = " Çalışma masanızda nelere dikkat edersiniz?";
            SoruSikki[14] = " Ertesi gün çözülmesi gereken bir problem varsa o akşamki ruh haliniz nasıl olur ?";

            Asikki[0] = "Güçlü, kararlı, girişken ve doğuştan liderim Düşer kalkar, yoluma devam ederim";
            Bsikki[0] = "Hayata anlamlı renkler katar eğlenceyi severim Ömür boyu herkesin mutlu ve neşeli olmasını dilerim";
            Csikki[0] = "Her anımı huzurlu ve sakin geçirmek isterim Kavga gürültü sevmem, işlerimde en kolay yolu seçerim";
            Dsikki[0] = "Her şeyin mükemmel, düzgün, kusursuz olmasını isterim İlişkilerimde saygılı ve mesafeli olmayı severim";
            Asikki[1] = "Hızlı ve sonuca yönelik";
            Bsikki[1] = "Çok hızlı, heyecanlı ve eğlenceli";
            Csikki[1] = "Daha yavaş ve sakin";
            Dsikki[1] = "Normal ve söyleyeceklerimi aklımda tartarak";
            Asikki[2] = "Sonuçları düşünmek";
            Bsikki[2] = "Onaylanmak, takdir edilmek";
            Csikki[2] = "Guruptaki arkadaşlarımın desteği";
            Dsikki[2] = "Etkinlik, düzen ve disiplin";
            Asikki[3] = "Yoğun ve hızlıyımdır. Aynı anda birkaç iş bir arada yapabilirim.";
            Bsikki[3] = "Özgür bir ortamda çalışırım. İnsan ilişkileri odaklıyımdır.";
            Csikki[3] = "Ön planda olmayan; ama gruba her türlü desteği veren bir yapım vardır.";
            Dsikki[3] = "Ayrıntıları önemserim ve tek bir konuya odaklanarak çalışırım.";
            Asikki[4] = "Hızlı bir tempoda çalışır, çabuk karar almayı severim.";
            Bsikki[4] = "İşlerin, rutin ve sıkıcı olmadığı ortamlarda yüksek motivasyonla çalışırım.";
            Csikki[4] = "Nadiren aceleciyimdir. Geç de olsa üstlendiğim işi bitiririm.";
            Dsikki[4] = "Ayrıntılı düşünerek karar veririm. İş bitirici bir tempoyla çalışırım.";
            Asikki[5] = "Zaman israfı ve işlerin gecikmesi";
            Bsikki[5] = "Tekrar gerektiren işler ve monotonluk";
            Csikki[5] = "Çatışma ortamı ve anlaşmazlıklar";
            Dsikki[5] = "Yanılmak ve yapılan hatanın tekrarlanması";
            Asikki[6] = "Olaylara yön veren ve otoriteyi kullanan";
            Bsikki[6] = "İnsanları motive eden ve neşelendiren";
            Csikki[6] = "Uzlaştırıcı ve grup içindeki uyumu sağlayan";
            Dsikki[6] = "Bilgi sağlayıcı, araştırıcı ve olayları takip eden";
            Asikki[7] = "Olaylar üzerindeki güç ve kontrolümün azaldığını hissetmek";
            Bsikki[7] = "Sıkıcı, rutin işler yapılan bir ortamda bulunmak";
            Csikki[7] = "Beni aşacağını düşündüğüm sorumluluklar Üstlenmek";
            Dsikki[7] = "Düzensiz ortamlar ve eksik yapılan işler";
            Asikki[8] = "Bunu zaten hak ettiğimi düşünürüm.";
            Bsikki[8] = "Çok sevinirim ve sevincimi belli ederim.";
            Csikki[8] = "Hocama teşekkür eder ve saygı duyarım.";
            Dsikki[8] = "Hocamın nerede hata yaptığını merak eder, kağıdımı görmek isterim.";
            Asikki[9] = "Konunun ana hatlarının konuşulması için, sonucun belli olmasından hemen sonra çıkmayı tercih ederim.";
            Bsikki[9] = "Toplantı eğlenceli bir şekilde devam ettiğinde sonuna kadar kalmayı, sıkıcı olmaya başladığında erken çıkmayı tercih ederim.";
            Csikki[9] = "Toplantının huzur içinde geçmesi ve güzel kararlar çıkması için üstüme düşeni yaparım.";
            Dsikki[9] = "Toplantıya vaktinden önce gelir, tüm detayları not eder ve bir değerlendirme yapmak için geç çıkarım.";
            Asikki[10] = "İnsanlar işleri istediğim gibi yapmadığında ve seri olmadıklarında sinirlenmek";
            Bsikki[10] = "Düzensiz, dağınık ve programsız olmak";
            Csikki[10] = "Kimseye hayır diyememek ve başkalarının işine koşarken kendi işimi aksatmak";
            Dsikki[10] = "Her şeyin kusursuz ve mükemmel olmasını istemek, İnsanlar buna uymadığında sinirlenmek";
            Asikki[11] = "Kısa sürede karar alan ve harekete geçen yapım";
            Bsikki[11] = "Girdiğim ortamlara neşe ve heyecan katan tarafım";
            Csikki[11] = "Her türlü ortama uyum sağlama ve çatışmaları önleme gayretim";
            Dsikki[11] = "Her şeyi planlı, programlı ve düzenli yapma huyum";
            Asikki[12] = "Güçlü, kararlı, otoriter ve yönlendirici";
            Bsikki[12] = "Popüler, neşeli, sevimli ve muzip";
            Csikki[12] = "Barışçıl, sevecen, uyumlu ve sakin";
            Dsikki[12] = "Tertipli, düzenli, disiplinli ve planlı";
            Asikki[13] = "Öncelik verdiğim şeyleri yapacak şekilde düzenlenmiş sade bir masayı tercih ederim.";
            Bsikki[13] = "İnsanlara karma karışık gelen; ama benim aradığım her şeyi bulduğum bir masada çalışırım.";
            Csikki[13] = "Önce masamın üzerine gerekli olan her türlü araç gereci koyarım. Sık sık kalkarak enerjimi harcamak istemem.";
            Dsikki[13] = "İyi bir iş çıkarmam için masam son derece derli-toplu ve düzenli olmalıdır.";
            Asikki[14] = "Çok büyük tedirginlik duymam; çünkü ertesi gün o işi olması gerektiği gibi yapacağımdan eminimdir.";
            Bsikki[14] = "Çok tedirginlik duymam; çünkü nasıl olsa işler bir şekilde hallolacaktır.";
            Csikki[14] = "Sorumluluğun üstümde olmasından dolayı tedirginlik duyarım.";
            Dsikki[14] = "Tedirginlik duyarım ve gecenin büyük bir bölümünde problemin nasıl çözüleceğiyle ilgili planlar yaparım.";



        }


        #region X Silme İşlemleri

        private void btnSilA_Click(object sender, EventArgs e)
        {
            if (txtCevapA.TextLength == 2)         //cevap sayısı 3 kontrolll
            {
                x_sayisi[kacincisoru] -= 2;
                cift_x = false;
            }

            else if (txtCevapA.TextLength == 1)
                x_sayisi[kacincisoru] -= 1;
            txtCevapA.Text = "";
            cevap_Sayisi -= 1;
        }
        private void btnSilB_Click(object sender, EventArgs e)
        {
            if (txtCevapB.TextLength == 2)         //cevap sayısı 3 kontrolll
            {
                x_sayisi[kacincisoru] -= 2;
                cift_x = false;
            }
            else if (txtCevapB.TextLength == 1)
                x_sayisi[kacincisoru] -= 1;
            txtCevapB.Text = "";
            cevap_Sayisi -= 1;
        }

        private void btnSilC_Click(object sender, EventArgs e)
        {
            if (txtCevapC.TextLength == 2)         //cevap sayısı 3 kontrolll
            {
                x_sayisi[kacincisoru] -= 2;
                cift_x = false;
            }
            else if (txtCevapC.TextLength == 1)
                x_sayisi[kacincisoru] -= 1;
            txtCevapC.Text = "";
            cevap_Sayisi -= 1;
        }

        private void brnSilD_Click(object sender, EventArgs e)
        {
            if (txtCevapD.TextLength == 2)         //cevap sayısı 3 kontrolll
            {
                x_sayisi[kacincisoru] -= 2;
                cift_x = false;
            }
            else if (txtCevapD.TextLength == 1)
                x_sayisi[kacincisoru] -= 1;
            txtCevapD.Text = "";
            cevap_Sayisi -= 1;
        }
        #endregion

        #region X Koyma İşlemleri
        private void button1_Click(object sender, EventArgs e)
        {


            if (x_sayisi[kacincisoru] < 3)
            {
                if (txtCevapA.Text == "XX")
                {
                    MessageBox.Show("En fazla 2 X girebilirsiniz", "Uyarı !!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtCevapA.Text = "XX";

                }
                else
                {
                    txtCevapA.Text += "X";
                    x_sayisi[kacincisoru]++;
                    if (txtCevapA.TextLength == 1)
                    {
                        cevap_Sayisi++;
                    }
                    else if (txtCevapA.TextLength == 2)
                    {
                        cift_x = true;
                    }


                }

            }



        }
        private void btnXB_Click(object sender, EventArgs e)
        {


            if (x_sayisi[kacincisoru] < 3)
            {
                if (txtCevapB.Text == "XX")
                {
                    MessageBox.Show("En fazla 2 X girebilirsiniz", "Uyarı !!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtCevapB.Text = "XX";

                }
                else
                {
                    txtCevapB.Text += "X";
                    x_sayisi[kacincisoru]++;
                    if (txtCevapB.TextLength == 1)
                    {
                        cevap_Sayisi++;
                    }
                    else if (txtCevapB.TextLength == 2)
                    {
                        cift_x = true;
                    }

                }
            }



        }

        private void btnXC_Click(object sender, EventArgs e)
        {

            if (x_sayisi[kacincisoru] < 3)
            {
                if (txtCevapC.Text == "XX")
                {
                    MessageBox.Show("En fazla 2 X girebilirsiniz", "Uyarı !!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtCevapC.Text = "XX";

                }
                else
                {
                    txtCevapC.Text += "X";
                    x_sayisi[kacincisoru]++;
                    if (txtCevapC.TextLength == 1)
                    {
                        cevap_Sayisi++;
                    }
                    else if (txtCevapC.TextLength == 2)
                    {
                        cift_x = true;
                    }

                }
            }




        }

        private void btnXD_Click(object sender, EventArgs e)
        {


            if (x_sayisi[kacincisoru] < 3)
            {
                if (txtCevapD.Text == "XX")
                {
                    MessageBox.Show("En fazla 2 X girebilirsiniz", "Uyarı !!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtCevapD.Text = "XX";

                }
                else
                {
                    txtCevapD.Text += "X";
                    x_sayisi[kacincisoru]++;
                    if (txtCevapD.TextLength == 1)
                    {
                        cevap_Sayisi++;
                    }
                    else if (txtCevapD.TextLength == 2)
                    {
                        cift_x = true;
                    }
                }
            }


        }
        #endregion        
        private void btnCikis_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Çıkmak istediğinizden emin misiniz ?", "UYARI !!!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void btnBitir_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Testi bitirmek istediğinizden emin misiniz ?", "Uyarı !!!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                //int enbuyuk = 0;
                for (int i = 0; i < 15; i++)
                {
                    toplamA = toplamA + Acevap[i].Length;
                    toplamB = toplamB + Bcevap[i].Length;
                    toplamC = toplamC + Ccevap[i].Length;
                    toplamD = toplamD + Dcevap[i].Length;
                }
                toplamsonuc = toplamA + toplamB + toplamC + toplamD;
                frmSonuc sonuc = new frmSonuc();

                //deg1 = (toplamA / toplamsonuc) * 360;
                //deg2 = (toplamB / toplamsonuc) * 360;
                //deg3 = (toplamC / toplamsonuc) * 360;
                //deg4 = (toplamD / toplamsonuc) * 360;
                //sonuc.deg1 = deg1;
                //sonuc.deg2 = deg2;
                //sonuc.deg3 = deg3;
                //sonuc.deg4 = deg4;
                sonuc.lblA.Text = toplamA.ToString();
                sonuc.lblB.Text = toplamB.ToString();
                sonuc.lblC.Text = toplamC.ToString();
                sonuc.lblD.Text = toplamD.ToString();
                frmSonuc.not1 = toplamA;
                frmSonuc.not2 = toplamB;
                frmSonuc.not3 = toplamC;
                frmSonuc.not4 = toplamD;


                int[] sonuclar = new int[4];
                sonuclar[0] = toplamA;
                sonuclar[1] = toplamB;
                sonuclar[2] = toplamC;
                sonuclar[3] = toplamD;
                
                //enbuyuk= toplamA;
                for (int i = 0; i < 4; i++)
                {
                    if (sonuclar[i] > enbuyuk)
                    {
                        siklar = i;         // 0=>A,    1=>B,   2=>C,   3=>D
                        enbuyuk = sonuclar[i];

                    }
                }

                this.Hide();
                sonuc.Show();
                switch (siklar)
                {
                    case 0:
                        {
                            richTextBox_A.Show();
                            richTextBox_B.Hide();
                            richTextBox_C.Hide();
                            richTextBox_D.Hide();
                            break;
                        }
                    case 1:
                        {
                            richTextBox_B.Visible = true;
                            richTextBox_A.Visible = false;
                            richTextBox_C.Visible = false;
                            richTextBox_D.Visible = false;
                            break;
                        }
                    case 2:
                        {
                            richTextBox_C.Visible = true;
                            richTextBox_A.Visible = false;
                            richTextBox_B.Visible = false;
                            richTextBox_D.Visible = false;
                            break;
                        }
                    case 3:
                        {
                            richTextBox_D.Visible = true;
                            richTextBox_A.Visible = false;
                            richTextBox_B.Visible = false;
                            richTextBox_C.Visible = false;
                            break;
                        }
                }

                this.Hide();
                sonuc.Show();
            }
        } 
        

        #region Cevaplar Değştiğinde

        private void txtCevapA_TextChanged(object sender, EventArgs e)
        {
            Acevap[kacincisoru] = txtCevapA.Text;

        }

        private void txtCevapB_TextChanged(object sender, EventArgs e)
        {
            Bcevap[kacincisoru] = txtCevapB.Text;

        }

        private void txtCevapC_TextChanged(object sender, EventArgs e)
        {
            Ccevap[kacincisoru] = txtCevapC.Text;

        }

        private void txtCevapD_TextChanged(object sender, EventArgs e)
        {
            Dcevap[kacincisoru] = txtCevapD.Text;

        }
        #endregion

        #region Kontroller
        private void x_sayisi_atama()
        {
            for (int i = 0; i < 15; i++)
                x_sayisi[i] = 0;
        }


        private bool cevap_sayisi_kontrol()
        {

            if (cevap_Sayisi > 2)
                return false;
            else
                return true;

        } 
        #endregion

        private void timer1_Tick(object sender, EventArgs e) // x 3 yerde işaretlenince next tusu kaybolur
        {
            if (cevap_Sayisi > 2 || (cift_x==false) )
                button_NEXT.Visible = false;
            else
                button_NEXT.Visible = true;
            if(kacincisoru==14)
            {
                button_NEXT.Hide();
                if (cevap_Sayisi > 2 || (cift_x == false))
                    button_bitir.Hide();
                else
                    button_bitir.Show();

            }
            else if(kacincisoru!=14)
            {
                button_NEXT.Show();
                button_bitir.Hide();
            }
            if (kacincisoru <= 0)
            {
                button_BACK.Hide();
            }
            else if(kacincisoru >0)
                button_BACK.Show();

        }
    }
}
