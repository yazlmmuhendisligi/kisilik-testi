﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KisilikTesti
{
    public partial class frmSonuc : Form
    {
        public float deg1, deg2, deg3, deg4;
        public static int  not2, not3, not4,not1;

        private void btnOku_Click(object sender, EventArgs e)
        {
            frmYorum frm = new frmYorum();
            frm.dizi2[0] = not1;
            frm.dizi2[1] = not2;
            frm.dizi2[2] = not3;
            frm.dizi2[3] = not4;
            frmYorum.not1 = not1;
            frmYorum.not2 = not2;
            frmYorum.not3 = not3;
            frmYorum.not4 = not4;
            this.Hide();
            frm.Show();
        }

        // Çıkış işlemleri
        private void btnCikis_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("Çıkmak İstediğinizden emin misiniz?","Dikkat!!!",MessageBoxButtons.YesNo,MessageBoxIcon.Warning)==DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GrafikCiz();
        }

        public frmSonuc()
        {
            InitializeComponent();
            GrafikCiz();

        }
        private void frmSonuc_Load(object sender, EventArgs e)
        {
            GrafikCiz();
        }

        private void GrafikCiz()
        {
            //deg1 = Convert.ToInt32(lblA.Text);
            //deg2 = Convert.ToInt32(lblB.Text);
            //deg3 = Convert.ToInt32(lblC.Text);
            //deg4 = Convert.ToInt32(lblD.Text);
            
            float toplam = not1 + not2 + not3 + not4;

            deg1 = (not1 / toplam) * 360;
            deg2 = (not2 / toplam) * 360;
            deg3 = (not3 / toplam) * 360;
            deg4 = (not4 / toplam) * 360;

            Pen p = new Pen(Color.Black, 1);
            Graphics g = this.CreateGraphics();
            Rectangle rec = new Rectangle(180, 75, 400, 400);


            Brush b1 = new SolidBrush(Color.Yellow);
            Brush b2 = new SolidBrush(Color.Blue);
            Brush b3 = new SolidBrush(Color.Red);
            Brush b4 = new SolidBrush(Color.Green);
            g.Clear(this.BackColor);
            g.DrawPie(p, rec, 0, deg1);
            g.FillPie(b1, rec, 0, deg1);
            g.DrawPie(p, rec, deg1, deg2);
            g.FillPie(b2, rec, deg1, deg2);
            g.DrawPie(p, rec, deg1 + deg2, deg3);
            g.FillPie(b3, rec, deg1 + deg2, deg3);
            g.DrawPie(p, rec, deg1 + deg2 + deg3, deg4);
            g.FillPie(b4, rec, deg1 + deg2 + deg3, deg4);

        }

    }
}
            
        
