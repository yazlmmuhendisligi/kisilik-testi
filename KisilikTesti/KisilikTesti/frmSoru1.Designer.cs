﻿namespace KisilikTesti
{
    partial class frmSoru1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Soru_label = new System.Windows.Forms.Label();
            this.txtCevapA = new System.Windows.Forms.TextBox();
            this.btnSilA = new System.Windows.Forms.Button();
            this.btnSilC = new System.Windows.Forms.Button();
            this.btnSilB = new System.Windows.Forms.Button();
            this.brnSilD = new System.Windows.Forms.Button();
            this.txtCevapB = new System.Windows.Forms.TextBox();
            this.txtCevapC = new System.Windows.Forms.TextBox();
            this.txtCevapD = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnXB = new System.Windows.Forms.Button();
            this.btnXC = new System.Windows.Forms.Button();
            this.btnXD = new System.Windows.Forms.Button();
            this.button_NEXT = new System.Windows.Forms.Button();
            this.button_BACK = new System.Windows.Forms.Button();
            this.richTextBox_A = new System.Windows.Forms.RichTextBox();
            this.richTextBox_B = new System.Windows.Forms.RichTextBox();
            this.richTextBox_C = new System.Windows.Forms.RichTextBox();
            this.richTextBox_D = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnCikis = new System.Windows.Forms.Button();
            this.btnBitir = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.button_bitir = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // Soru_label
            // 
            this.Soru_label.AutoSize = true;
            this.Soru_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Soru_label.ForeColor = System.Drawing.Color.Green;
            this.Soru_label.Location = new System.Drawing.Point(39, 77);
            this.Soru_label.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Soru_label.Name = "Soru_label";
            this.Soru_label.Size = new System.Drawing.Size(90, 26);
            this.Soru_label.TabIndex = 0;
            this.Soru_label.Text = "Soru 1)";
            // 
            // txtCevapA
            // 
            this.txtCevapA.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txtCevapA.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCevapA.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtCevapA.Location = new System.Drawing.Point(802, 161);
            this.txtCevapA.Margin = new System.Windows.Forms.Padding(2);
            this.txtCevapA.Multiline = true;
            this.txtCevapA.Name = "txtCevapA";
            this.txtCevapA.ReadOnly = true;
            this.txtCevapA.Size = new System.Drawing.Size(50, 65);
            this.txtCevapA.TabIndex = 3;
            this.txtCevapA.TextChanged += new System.EventHandler(this.txtCevapA_TextChanged);
            // 
            // btnSilA
            // 
            this.btnSilA.BackColor = System.Drawing.Color.Transparent;
            this.btnSilA.BackgroundImage = global::KisilikTesti.Properties.Resources.sil;
            this.btnSilA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSilA.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSilA.Location = new System.Drawing.Point(916, 169);
            this.btnSilA.Margin = new System.Windows.Forms.Padding(2);
            this.btnSilA.Name = "btnSilA";
            this.btnSilA.Size = new System.Drawing.Size(50, 46);
            this.btnSilA.TabIndex = 4;
            this.btnSilA.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnSilA.UseVisualStyleBackColor = false;
            this.btnSilA.Click += new System.EventHandler(this.btnSilA_Click);
            // 
            // btnSilC
            // 
            this.btnSilC.BackColor = System.Drawing.Color.Transparent;
            this.btnSilC.BackgroundImage = global::KisilikTesti.Properties.Resources.sil;
            this.btnSilC.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSilC.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSilC.Location = new System.Drawing.Point(916, 317);
            this.btnSilC.Margin = new System.Windows.Forms.Padding(2);
            this.btnSilC.Name = "btnSilC";
            this.btnSilC.Size = new System.Drawing.Size(50, 46);
            this.btnSilC.TabIndex = 4;
            this.btnSilC.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnSilC.UseVisualStyleBackColor = false;
            this.btnSilC.Click += new System.EventHandler(this.btnSilC_Click);
            // 
            // btnSilB
            // 
            this.btnSilB.BackColor = System.Drawing.Color.Transparent;
            this.btnSilB.BackgroundImage = global::KisilikTesti.Properties.Resources.sil;
            this.btnSilB.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSilB.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSilB.Location = new System.Drawing.Point(916, 240);
            this.btnSilB.Margin = new System.Windows.Forms.Padding(2);
            this.btnSilB.Name = "btnSilB";
            this.btnSilB.Size = new System.Drawing.Size(50, 46);
            this.btnSilB.TabIndex = 4;
            this.btnSilB.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnSilB.UseVisualStyleBackColor = false;
            this.btnSilB.Click += new System.EventHandler(this.btnSilB_Click);
            // 
            // brnSilD
            // 
            this.brnSilD.BackColor = System.Drawing.Color.Transparent;
            this.brnSilD.BackgroundImage = global::KisilikTesti.Properties.Resources.sil;
            this.brnSilD.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.brnSilD.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.brnSilD.Location = new System.Drawing.Point(916, 393);
            this.brnSilD.Margin = new System.Windows.Forms.Padding(2);
            this.brnSilD.Name = "brnSilD";
            this.brnSilD.Size = new System.Drawing.Size(50, 46);
            this.brnSilD.TabIndex = 4;
            this.brnSilD.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.brnSilD.UseVisualStyleBackColor = false;
            this.brnSilD.Click += new System.EventHandler(this.brnSilD_Click);
            // 
            // txtCevapB
            // 
            this.txtCevapB.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txtCevapB.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCevapB.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtCevapB.Location = new System.Drawing.Point(802, 232);
            this.txtCevapB.Margin = new System.Windows.Forms.Padding(2);
            this.txtCevapB.Multiline = true;
            this.txtCevapB.Name = "txtCevapB";
            this.txtCevapB.ReadOnly = true;
            this.txtCevapB.Size = new System.Drawing.Size(51, 65);
            this.txtCevapB.TabIndex = 5;
            this.txtCevapB.TextChanged += new System.EventHandler(this.txtCevapB_TextChanged);
            // 
            // txtCevapC
            // 
            this.txtCevapC.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txtCevapC.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCevapC.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtCevapC.Location = new System.Drawing.Point(803, 310);
            this.txtCevapC.Margin = new System.Windows.Forms.Padding(2);
            this.txtCevapC.Multiline = true;
            this.txtCevapC.Name = "txtCevapC";
            this.txtCevapC.ReadOnly = true;
            this.txtCevapC.Size = new System.Drawing.Size(50, 65);
            this.txtCevapC.TabIndex = 6;
            this.txtCevapC.TextChanged += new System.EventHandler(this.txtCevapC_TextChanged);
            // 
            // txtCevapD
            // 
            this.txtCevapD.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txtCevapD.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCevapD.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtCevapD.Location = new System.Drawing.Point(803, 386);
            this.txtCevapD.Margin = new System.Windows.Forms.Padding(2);
            this.txtCevapD.Multiline = true;
            this.txtCevapD.Name = "txtCevapD";
            this.txtCevapD.ReadOnly = true;
            this.txtCevapD.Size = new System.Drawing.Size(50, 65);
            this.txtCevapD.TabIndex = 7;
            this.txtCevapD.TextChanged += new System.EventHandler(this.txtCevapD_TextChanged);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Silver;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(857, 161);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(54, 63);
            this.button1.TabIndex = 8;
            this.button1.Text = "X";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnXB
            // 
            this.btnXB.BackColor = System.Drawing.Color.Silver;
            this.btnXB.Font = new System.Drawing.Font("Microsoft Sans Serif", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnXB.ForeColor = System.Drawing.Color.Black;
            this.btnXB.Location = new System.Drawing.Point(857, 230);
            this.btnXB.Name = "btnXB";
            this.btnXB.Size = new System.Drawing.Size(54, 67);
            this.btnXB.TabIndex = 9;
            this.btnXB.Text = "X";
            this.btnXB.UseVisualStyleBackColor = false;
            this.btnXB.Click += new System.EventHandler(this.btnXB_Click);
            // 
            // btnXC
            // 
            this.btnXC.BackColor = System.Drawing.Color.Silver;
            this.btnXC.Font = new System.Drawing.Font("Microsoft Sans Serif", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnXC.ForeColor = System.Drawing.Color.Black;
            this.btnXC.Location = new System.Drawing.Point(857, 306);
            this.btnXC.Name = "btnXC";
            this.btnXC.Size = new System.Drawing.Size(54, 69);
            this.btnXC.TabIndex = 10;
            this.btnXC.Text = "X";
            this.btnXC.UseVisualStyleBackColor = false;
            this.btnXC.Click += new System.EventHandler(this.btnXC_Click);
            // 
            // btnXD
            // 
            this.btnXD.BackColor = System.Drawing.Color.Silver;
            this.btnXD.Font = new System.Drawing.Font("Microsoft Sans Serif", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnXD.ForeColor = System.Drawing.Color.Black;
            this.btnXD.Location = new System.Drawing.Point(858, 381);
            this.btnXD.Name = "btnXD";
            this.btnXD.Size = new System.Drawing.Size(53, 70);
            this.btnXD.TabIndex = 11;
            this.btnXD.Text = "X";
            this.btnXD.UseVisualStyleBackColor = false;
            this.btnXD.Click += new System.EventHandler(this.btnXD_Click);
            // 
            // button_NEXT
            // 
            this.button_NEXT.BackColor = System.Drawing.Color.YellowGreen;
            this.button_NEXT.Font = new System.Drawing.Font("Showcard Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_NEXT.ForeColor = System.Drawing.SystemColors.Desktop;
            this.button_NEXT.Location = new System.Drawing.Point(524, 484);
            this.button_NEXT.Name = "button_NEXT";
            this.button_NEXT.Size = new System.Drawing.Size(118, 49);
            this.button_NEXT.TabIndex = 12;
            this.button_NEXT.Text = "NEXT";
            this.button_NEXT.UseVisualStyleBackColor = false;
            this.button_NEXT.Click += new System.EventHandler(this.button_NEXT_Click);
            // 
            // button_BACK
            // 
            this.button_BACK.BackColor = System.Drawing.Color.YellowGreen;
            this.button_BACK.Font = new System.Drawing.Font("Showcard Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_BACK.ForeColor = System.Drawing.SystemColors.Desktop;
            this.button_BACK.Location = new System.Drawing.Point(400, 484);
            this.button_BACK.Name = "button_BACK";
            this.button_BACK.Size = new System.Drawing.Size(118, 49);
            this.button_BACK.TabIndex = 13;
            this.button_BACK.Text = "BACK";
            this.button_BACK.UseVisualStyleBackColor = false;
            this.button_BACK.Click += new System.EventHandler(this.button_BACK_Click);
            // 
            // richTextBox_A
            // 
            this.richTextBox_A.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.richTextBox_A.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox_A.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox_A.Location = new System.Drawing.Point(132, 161);
            this.richTextBox_A.Name = "richTextBox_A";
            this.richTextBox_A.ReadOnly = true;
            this.richTextBox_A.Size = new System.Drawing.Size(665, 65);
            this.richTextBox_A.TabIndex = 14;
            this.richTextBox_A.Text = "";
            // 
            // richTextBox_B
            // 
            this.richTextBox_B.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.richTextBox_B.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox_B.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox_B.Location = new System.Drawing.Point(132, 232);
            this.richTextBox_B.Name = "richTextBox_B";
            this.richTextBox_B.ReadOnly = true;
            this.richTextBox_B.Size = new System.Drawing.Size(665, 65);
            this.richTextBox_B.TabIndex = 15;
            this.richTextBox_B.Text = "";
            // 
            // richTextBox_C
            // 
            this.richTextBox_C.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.richTextBox_C.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox_C.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox_C.Location = new System.Drawing.Point(133, 310);
            this.richTextBox_C.Name = "richTextBox_C";
            this.richTextBox_C.ReadOnly = true;
            this.richTextBox_C.Size = new System.Drawing.Size(665, 65);
            this.richTextBox_C.TabIndex = 16;
            this.richTextBox_C.Text = "";
            // 
            // richTextBox_D
            // 
            this.richTextBox_D.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.richTextBox_D.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox_D.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox_D.Location = new System.Drawing.Point(133, 386);
            this.richTextBox_D.Name = "richTextBox_D";
            this.richTextBox_D.ReadOnly = true;
            this.richTextBox_D.Size = new System.Drawing.Size(665, 65);
            this.richTextBox_D.TabIndex = 17;
            this.richTextBox_D.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(91, 161);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 25);
            this.label1.TabIndex = 18;
            this.label1.Text = "A)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(92, 230);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 25);
            this.label2.TabIndex = 19;
            this.label2.Text = "B)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(90, 306);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 25);
            this.label3.TabIndex = 20;
            this.label3.Text = "C)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(91, 385);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 25);
            this.label4.TabIndex = 21;
            this.label4.Text = "D)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label5.Location = new System.Drawing.Point(138, 108);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 24);
            this.label5.TabIndex = 22;
            this.label5.Text = "label5";
            // 
            // btnCikis
            // 
            this.btnCikis.BackgroundImage = global::KisilikTesti.Properties.Resources.cikis;
            this.btnCikis.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCikis.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCikis.Location = new System.Drawing.Point(52, 572);
            this.btnCikis.Margin = new System.Windows.Forms.Padding(2);
            this.btnCikis.Name = "btnCikis";
            this.btnCikis.Size = new System.Drawing.Size(53, 59);
            this.btnCikis.TabIndex = 23;
            this.btnCikis.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCikis.UseVisualStyleBackColor = true;
            this.btnCikis.Click += new System.EventHandler(this.btnCikis_Click);
            // 
            // btnBitir
            // 
            this.btnBitir.BackgroundImage = global::KisilikTesti.Properties.Resources.bitir1;
            this.btnBitir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBitir.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBitir.Location = new System.Drawing.Point(881, 572);
            this.btnBitir.Margin = new System.Windows.Forms.Padding(2);
            this.btnBitir.Name = "btnBitir";
            this.btnBitir.Size = new System.Drawing.Size(71, 59);
            this.btnBitir.TabIndex = 24;
            this.btnBitir.UseVisualStyleBackColor = true;
            this.btnBitir.Click += new System.EventHandler(this.btnBitir_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // button_bitir
            // 
            this.button_bitir.BackgroundImage = global::KisilikTesti.Properties.Resources.bitir1;
            this.button_bitir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button_bitir.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button_bitir.Location = new System.Drawing.Point(553, 481);
            this.button_bitir.Margin = new System.Windows.Forms.Padding(2);
            this.button_bitir.Name = "button_bitir";
            this.button_bitir.Size = new System.Drawing.Size(71, 59);
            this.button_bitir.TabIndex = 24;
            this.button_bitir.UseVisualStyleBackColor = true;
            this.button_bitir.Click += new System.EventHandler(this.btnBitir_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.richTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox1.Location = new System.Drawing.Point(134, 59);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(664, 96);
            this.richTextBox1.TabIndex = 25;
            this.richTextBox1.Text = "";
            // 
            // frmSoru1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.Silver;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1106, 663);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.button_bitir);
            this.Controls.Add(this.btnBitir);
            this.Controls.Add(this.btnCikis);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.richTextBox_D);
            this.Controls.Add(this.richTextBox_C);
            this.Controls.Add(this.richTextBox_B);
            this.Controls.Add(this.richTextBox_A);
            this.Controls.Add(this.button_BACK);
            this.Controls.Add(this.button_NEXT);
            this.Controls.Add(this.btnXD);
            this.Controls.Add(this.btnXC);
            this.Controls.Add(this.btnXB);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtCevapD);
            this.Controls.Add(this.txtCevapC);
            this.Controls.Add(this.txtCevapB);
            this.Controls.Add(this.btnSilB);
            this.Controls.Add(this.brnSilD);
            this.Controls.Add(this.btnSilC);
            this.Controls.Add(this.btnSilA);
            this.Controls.Add(this.txtCevapA);
            this.Controls.Add(this.Soru_label);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmSoru1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Soru 1";
            this.Load += new System.EventHandler(this.frmSoru1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Soru_label;
        private System.Windows.Forms.TextBox txtCevapA;
        private System.Windows.Forms.Button btnSilA;
        private System.Windows.Forms.Button btnSilC;
        private System.Windows.Forms.Button btnSilB;
        private System.Windows.Forms.Button brnSilD;
        private System.Windows.Forms.TextBox txtCevapB;
        private System.Windows.Forms.TextBox txtCevapC;
        private System.Windows.Forms.TextBox txtCevapD;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnXB;
        private System.Windows.Forms.Button btnXC;
        private System.Windows.Forms.Button btnXD;
        private System.Windows.Forms.Button button_NEXT;
        private System.Windows.Forms.Button button_BACK;
        private System.Windows.Forms.RichTextBox richTextBox_A;
        private System.Windows.Forms.RichTextBox richTextBox_B;
        private System.Windows.Forms.RichTextBox richTextBox_C;
        private System.Windows.Forms.RichTextBox richTextBox_D;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnCikis;
        private System.Windows.Forms.Button btnBitir;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button button_bitir;
        private System.Windows.Forms.RichTextBox richTextBox1;
    }
}